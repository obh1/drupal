FROM drupal as base
WORKDIR /drupal
RUN rm -rf /var/www/html
RUN ln -s /drupal/web /var/www/html
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
 && php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
 && php composer-setup.php \
 && php -r "unlink('composer-setup.php');" \
 && mv composer.phar /usr/bin/composer
ENV COMPOSER_CACHE_DIR /composer
COPY drupal/composer.json /tmp/c/composer.json
COPY drupal/composer.lock /tmp/c/composer.lock
RUN cd /tmp/c/ && composer install && rm -rf /tmp/c/

COPY --chown=www-data:www-data settings.php /drupal/web/sites/default/settings.php
COPY --chown=www-data:www-data ./modules /drupal/web/modules/custom
COPY --chown=www-data:www-data ./themes /drupal/web/themes/custom
COPY --chown=www-data:www-data ./profiles /drupal/web/profiles/custom

FROM base as live
COPY drupal/composer.json /drupal/composer.json
COPY drupal/composer.lock /drupal/composer.lock
RUN composer install --no-dev

FROM base as dev
RUN apt-get update && apt-get install -y git
COPY drupal/composer.json /drupal/composer.json
COPY drupal/composer.lock /drupal/composer.lock
RUN composer install
COPY php.ini /usr/local/etc/php/php.ini


